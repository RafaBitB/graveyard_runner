﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFundo : MonoBehaviour {

    private float larguraTela;
    private bool vivo;

    private void Start()
    {
        vivo = true;
        AjustaBackGround();
        if(this.name == "BgDois")
        {
            this.transform.position = new Vector2(larguraTela, 0);
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(-1.5f, 0);
    }
    private void Update()
    {
        if (vivo)
        {
            if (this.transform.position.x <= -larguraTela)
            {
                this.transform.position = new Vector2(larguraTela, 0);
            }
        }
        
    }
    private void AjustaBackGround()
    {
        //Identificando altura/largura da imagem(BackGround)
        SpriteRenderer grafico = GetComponent<SpriteRenderer>();
        float larguraImagem = grafico.sprite.bounds.size.x;
        float alturaImagem = grafico.sprite.bounds.size.y;


        //Identificando a proporção da tela que está sendo usada.
        float alturaTela = Camera.main.orthographicSize * 2;
        larguraTela = alturaTela / Screen.height * Screen.width;

        //Criando nova escala para a imagem de fundo.
        Vector2 novaEscala = transform.localScale;
        novaEscala.x = larguraTela / larguraImagem+0.05f;
        novaEscala.y = alturaTela / alturaImagem;

        this.transform.localScale = novaEscala;
    }
    void Fim()
    {
        vivo = false;
    }
}
