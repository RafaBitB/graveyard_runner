﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptEnemy : MonoBehaviour {

    public AudioClip somGemido;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D>().velocity = new Vector2(-4,0);
        GetComponent<AudioSource>().PlayOneShot(somGemido);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
