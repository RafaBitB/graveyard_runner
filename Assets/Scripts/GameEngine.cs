﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameEngine : MonoBehaviour {

	public List<GameObject> inimigos = new List<GameObject>();
	public GameObject hero;
	public Text pontuacao;
	public GameObject telaFim;
	private string texto;
	int score;

	System.Random random = new System.Random();
    private bool vivo;

    // Use this for initialization
    void Start () {
		telaFim.SetActive (false);
		Time.timeScale = 1;
        vivo = true;
		//pontuacao.transform.position = new Vector2 (Screen.width / 2, Screen.height / 2);
		pontuacao.text = "Pontos: ";
		pontuacao.fontSize = 75;
        if (vivo)
        {
            InvokeRepeating("CriaInimigo", 5f, 3.5f);
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void CriaInimigo(){
		
			float alturaAleatoria;
			GameObject inimigo = inimigos[random.Next(0, 3)].gameObject;

			if(inimigo.tag == "zombie"){

				alturaAleatoria = hero.transform.position.y;

			}else{

				alturaAleatoria = 10.0f * Random.value -5;
			}

			GameObject novoInimigo = Instantiate (inimigo);
			novoInimigo.transform.position = new Vector2(15.0f,alturaAleatoria);

	}
	IEnumerator Fim()
    {
		yield return new WaitForSeconds(1.5f);
        vivo = false;
		Time.timeScale = 0;
		telaFim.SetActive (true);

    }
	void MarcaPonto()
	{
		score += 50;
		pontuacao.text = score.ToString ();

	}
	public void JogarNovamente()
	{
		SceneManager.LoadScene ("Runner");
	}
	public void Sair()
	{
		Application.Quit();
	}

}
