﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptHero : MonoBehaviour {

    private Animator anima;
    private Rigidbody2D body;
    private AudioSource som;
    private float ladoHero;
    private bool vivo;
	private bool comecou;
    private bool zombieMorto;

    private MoveFundo fundo;
    private ScriptChao chao;
    public GameEngine ge;

    //Sounds
    public AudioClip somBate;
    public AudioClip somPula;
    public AudioClip somMorre;
    public AudioClip somMataZumbiHomem;
    public AudioClip somMataZumbiMulher;

    //Jump
    float posicaoInicial;
	bool jumping;

    // Use this for initialization
    void Start ()
    {
        anima = GetComponent<Animator>();
        body = GetComponent<Rigidbody2D>();
        som = GetComponent<AudioSource>();
        vivo = true;
        ladoHero = transform.localScale.x;
		posicaoInicial = transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
		Jump();
	}
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "zombie" && vivo && !zombieMorto) {
            if (body.position.y <= other.gameObject.transform.position.y)
            {
                anima.SetBool("damage", true);
                GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.35f, 0.35f);
                transform.localScale = new Vector2(-ladoHero, transform.localScale.y);
                som.PlayOneShot(somMorre);
                vivo = false;
				ge.SendMessage ("Fim");
            }
            else
            {
                body.AddForce(new Vector2(0, 350));
				ge.SendMessage("MarcaPonto");
                som.PlayOneShot(somBate);
                other.gameObject.GetComponent<Animator>().SetBool("hurt",true);
                other.transform.position = new Vector2(other.transform.position.x, other.transform.position.y-1.5f);
                
                if(other.gameObject.name.Contains("ZombieFemale"))
                {
                    som.PlayOneShot(somMataZumbiMulher);
                }
                else
                {
                    som.PlayOneShot(somMataZumbiHomem);
                }
                zombieMorto = true;
                StartCoroutine(ZombieDie(other));
            }
			
		}
    }
	void Jump(){
		if(transform.position.y <= posicaoInicial)
        {
            if (Input.GetButtonDown("Jump") || Input.GetButtonDown("Fire1"))
            {
				body.AddForce (new Vector2 (0, 450));
				jumping = true;
			}
        }else{
            jumping = false;
		}
        if (jumping && vivo)
        {
            anima.SetBool("jump", true);
            som.PlayOneShot(somPula);
        }
        else
        {
            anima.SetBool("jump", false);
        }
    }
    void Fim()
    {
        vivo = false;
        Invoke("RecarregaCena", 3);
    }
    void RecarregaCena()
    {
        Application.LoadLevel("Runner");
    }
    IEnumerator ZombieDie(Collision2D other)
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(other.gameObject);
        zombieMorto = false;
    }
}
