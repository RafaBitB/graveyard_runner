﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptChao : MonoBehaviour {

    private float larguraTela;
    private bool vivo;

    // Use this for initialization
    void Start () {
        vivo = true;
        AjustaTamanho();

        if (this.name == "ChaoDois")
        {
            this.transform.position = new Vector2(larguraTela, -4.95f);
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(-2.0f, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (vivo)
        {
            if (this.transform.position.x <= -larguraTela)
            {
                this.transform.position = new Vector2(larguraTela, -4.95f);
            }
        }
        
    }

    private void AjustaTamanho()
    {
        //Identificando altura/largura da imagem(BackGround)
        SpriteRenderer grafico = GetComponent<SpriteRenderer>();
        float larguraImagem = grafico.sprite.bounds.size.x;


        //Identificando a proporção da tela que está sendo usada.
        float alturaTela = Camera.main.orthographicSize * 2;
        larguraTela = alturaTela / Screen.height * Screen.width;

        //Criando nova escala para a imagem de fundo.
        Vector2 novaEscala = transform.localScale;
        novaEscala.x = larguraTela / larguraImagem + 0.05f;

        this.transform.localScale = novaEscala;
    }
    void Fim()
    {
        vivo = false;
    }
}
